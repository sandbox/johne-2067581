OpenAid Countries Extra

Description:
The Extra modules perform tasks that are not safe to include directly in Features code due to the possibility of overwrites.
The Atom Countries Extra module creates the predefined list of available countries.

Caveats:

